package br.com.harerama.b2w.recurso;

import br.com.harerama.b2w.modelo.Planeta;
import br.com.harerama.b2w.servico.ServicoPlaneta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

import static java.util.Objects.nonNull;

@RestController
@RequestMapping("/planetas")
public class RecursoPlaneta {

    @Autowired
    private ServicoPlaneta servicoPlaneta;

    @GetMapping
    public ResponseEntity<List<Planeta>> listar() {
        return ResponseEntity.ok(servicoPlaneta.listar());
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<Planeta> buscar(@PathVariable Long id) {
        Planeta planeta = servicoPlaneta.buscar(id);
        if(nonNull(planeta)){
            return ResponseEntity.status(HttpStatus.OK).body(planeta);
        }else{
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(path = "/nome/{nome}")
    public ResponseEntity<Planeta> buscarPorNome(@PathVariable String nome) {
        Planeta planeta = servicoPlaneta.buscarPorNome(nome);
        if(nonNull(planeta)){
            return ResponseEntity.status(HttpStatus.OK).body(planeta);
        }else{
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    public ResponseEntity<Void> salvar(@RequestBody @Valid Planeta planeta) {
        servicoPlaneta.salvar(planeta);

        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}").buildAndExpand(planeta.getId()).toUri();

        return ResponseEntity.created(uri).build();
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletar(@PathVariable Long id) {
        servicoPlaneta.deletar(id);
    }}
