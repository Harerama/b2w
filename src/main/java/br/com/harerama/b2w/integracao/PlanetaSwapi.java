package br.com.harerama.b2w.integracao;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.ListUtils;

import java.util.List;

@Getter @Setter
public class PlanetaSwapi {

    private String name;
    private String climate;
    private String terrain;

    @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
    private List<String> films;

    @JsonIgnore
    public int getQuantidadeDeFilmes() {
        return ListUtils.emptyIfNull(films).size();
    }
}
