package br.com.harerama.b2w.integracao;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter @Setter
public class Swapi {

    private String next;
    private List<PlanetaSwapi> results;

    public Swapi() {

    }

    public Swapi(String next) {
        this.next = next;
    }

}
