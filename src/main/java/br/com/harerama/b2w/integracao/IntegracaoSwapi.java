package br.com.harerama.b2w.integracao;

import br.com.harerama.b2w.modelo.Planeta;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class IntegracaoSwapi {

    private String url = "https://swapi.co/api/planets/?page=1";

    public List<Planeta> obterPlanetasPadroes() {

        List<Planeta> planetas = new ArrayList<>();

        do {
            Swapi swapi = realizarRequisicao(url);
            planetas.addAll(converterPlanetas(swapi.getResults()));
            url = swapi.getNext();
        } while (tiverProximaPagina());


        return planetas;
    }

    private Swapi realizarRequisicao(String url) {

        CloseableHttpClient httpClient = HttpClients.custom().setSSLHostnameVerifier(new NoopHostnameVerifier()).build();

        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setHttpClient(httpClient);

        RestTemplate restTemplate = new RestTemplate(requestFactory);

        RequestEntity<Void> request =
                RequestEntity.get(URI.create(url))
                        .accept(MediaType.APPLICATION_JSON)
                        .build();

        ResponseEntity<Swapi> response = restTemplate.exchange(request, Swapi.class);

        return response.getBody();
    }

    private boolean tiverProximaPagina() {
        return !StringUtils.isEmpty(url);
    }

    private List<Planeta> converterPlanetas(List<PlanetaSwapi> results) {
        List<Planeta> planetas = new ArrayList<>();
        results.forEach(ps -> {
            planetas.add(new Planeta(ps.getName(), ps.getClimate(), ps.getTerrain(), ps.getQuantidadeDeFilmes()));
        });
        return planetas;
    }

}
