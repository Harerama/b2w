package br.com.harerama.b2w.repositorio;

import br.com.harerama.b2w.modelo.Sequencia;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RepositorioSequencia extends MongoRepository<Sequencia, Long> {
    Sequencia findByCodigo(String codigo);
}
