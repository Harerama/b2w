package br.com.harerama.b2w.repositorio;

import br.com.harerama.b2w.modelo.Planeta;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface RepositorioPlaneta extends MongoRepository<Planeta, Long> {
    Optional<Planeta> findByNome(String nome);
}
