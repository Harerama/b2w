package br.com.harerama.b2w.modelo;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Document(collection = "sequencia")
public class Sequencia {

    @Id
    private Long id = 1L;
    private String codigo;
    private Long idSequencia = 0L;

    public Sequencia() {
    }

    public Sequencia(String codigo) {
        this.codigo = codigo;
    }

    public void atualizarId() {
        this.idSequencia += 1;
    }
}
