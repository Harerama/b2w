package br.com.harerama.b2w.modelo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Erro {

    private String mensagem;

    public Erro(String mensagem) {
        this.mensagem = mensagem;
    }
}
