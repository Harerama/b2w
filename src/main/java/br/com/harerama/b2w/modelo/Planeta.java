package br.com.harerama.b2w.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;

@Document(collection = "planeta")
public class Planeta extends EntidadeBase {

    private static final String CODIGO = "PLANETA";

    @Getter @Setter
    @NotBlank(message = "O campo nome deve ser preenchido")
    private String nome;

    @Getter @Setter
    @NotBlank(message = "O campo clima deve ser preenchido")
    private String clima;

    @Getter @Setter
    @NotBlank(message = "O campo terreno deve ser preenchido")
    private String terreno;

    @Getter @Setter
    private int qntFilmes;

    @Override
    public String getCodigo() {
        return CODIGO;
    }

    public Planeta() {
    }

    public Planeta(@NotBlank(message = "O campo nome deve ser preenchido") String nome,
                   @NotBlank(message = "O campo clima deve ser preenchido") String clima,
                   @NotBlank(message = "O campo terreno deve ser preenchido") String terreno,
                   int qntFilmes) {
        this.nome = nome;
        this.clima = clima;
        this.terreno = terreno;
        this.qntFilmes = qntFilmes;
    }

}
