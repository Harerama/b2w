package br.com.harerama.b2w.modelo;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;

public abstract class EntidadeBase {

    @Getter @Setter
    @Id
    private Long id;

    public abstract String getCodigo();

}
