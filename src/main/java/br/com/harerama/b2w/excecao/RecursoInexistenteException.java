package br.com.harerama.b2w.excecao;

public class RecursoInexistenteException extends RuntimeException {

    public RecursoInexistenteException(String message) {
        super(message);
    }
}
