package br.com.harerama.b2w.excecao;

import br.com.harerama.b2w.modelo.Erro;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Arrays;

@ControllerAdvice
public class ManipuladorExcecao extends ResponseEntityExceptionHandler {

    @ExceptionHandler(RecursoInexistenteException.class)
    public ResponseEntity<?> handleEmptyResultDataAccessException(RecursoInexistenteException ex, WebRequest request) {
        return super.handleExceptionInternal(ex, Arrays.asList(new Erro("Recurso inexistente")), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

}
