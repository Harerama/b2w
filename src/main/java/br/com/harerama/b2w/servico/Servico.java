package br.com.harerama.b2w.servico;

import br.com.harerama.b2w.modelo.EntidadeBase;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class Servico {

    @Autowired
    private ServicoSequencia servicoSequencia;

    public void salvar(EntidadeBase entidadeBase) {
        Long id = servicoSequencia.proximoId(entidadeBase.getCodigo());
        entidadeBase.setId(id);
    }

}
