package br.com.harerama.b2w.servico;

import br.com.harerama.b2w.modelo.Sequencia;
import br.com.harerama.b2w.repositorio.RepositorioSequencia;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static java.util.Objects.isNull;

@Service
public class ServicoSequencia {

    @Autowired
    private RepositorioSequencia repositorioSequencia;

    public Long proximoId(String codigo) {
        Sequencia sequencia = repositorioSequencia.findByCodigo(codigo);
        if(isNull(sequencia)){
            sequencia = new Sequencia(codigo);
        }
        sequencia.atualizarId();
        repositorioSequencia.save(sequencia);
        return sequencia.getIdSequencia();
    }

}
