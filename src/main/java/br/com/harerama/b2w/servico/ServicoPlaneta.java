package br.com.harerama.b2w.servico;

import br.com.harerama.b2w.excecao.RecursoInexistenteException;
import br.com.harerama.b2w.modelo.Planeta;
import br.com.harerama.b2w.repositorio.RepositorioPlaneta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Service
public class ServicoPlaneta extends Servico {

    @Autowired
    private RepositorioPlaneta repositorioPlaneta;

    public void salvar(Planeta planeta) {
        super.salvar(planeta);
        repositorioPlaneta.save(planeta);
    }

    public List<Planeta> listar() {
        return repositorioPlaneta.findAll();
    }

    public Planeta buscar(Long id) {
        return repositorioPlaneta.findById(id).orElseThrow(() -> new RecursoInexistenteException("Recurso inexistente"));
    }

    public void deletar(Long id) {
        repositorioPlaneta.deleteById(id);
    }

    public Planeta buscarPorNome(String nome) {
        return repositorioPlaneta.findByNome(nome).orElseThrow(() -> new RecursoInexistenteException("Recurso inexistente"));
    }

}
