package br.com.harerama.b2w;

import br.com.harerama.b2w.servico.ServicoConstrucao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class B2WApplication {

	@Autowired
	private ServicoConstrucao servicoConstrucao;

	public static void main(String[] args) {
		SpringApplication.run(B2WApplication.class, args);
	}

	@PostConstruct
	private void init() {
		servicoConstrucao.alimentarBancoDeDadosComPlanetas();
	}

}
